# Django
* Al registrar un usuario, considerar no devolver la información del usuario, en su lugar buscar si tal vez existe un método
que genere un token a partir de las credenciales recibidas para la creación del usuario, tal vez
[esta documentación](https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)
sea de ayuda.
* Django está guardando los números de teléfono como BigIntegerfield, pero al exponer los datos, esto puede presentar
problemas si el número es demasiado largo, así que sería una buena idea hacer un parse a String al exponer este dato.

# API Gateway
* Si se implementa el retorno del token al momento de registrar un usuario, el resolver en el API Gateway no necesitaría
hacer el login (ya que es lo que actualmente hace para retornar el token), ya que simplemente el registro del usuario
retornaría el token.
* Si se implementa el parsing a String del número de teléfono en django, se debe modificar el tipo UserInformation, que
actualmente define el campo phoneNumber como un número entero (Int).